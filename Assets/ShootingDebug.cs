using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShootingDebug : MonoBehaviour
{
    public GameObject playerObject;

    public GameObject projectilePrefab;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerObject == null)
        {
            playerObject = GameObject.FindGameObjectsWithTag("Player").FirstOrDefault(t => t.GetComponent<FirstPersonController>());
        }
        else
        {
            Vector3 crossVector = Vector3
                .Cross(playerObject.transform.position - transform.position, Vector3.up).normalized;
            //Debug.DrawRay(playerObject.transform.position, crossVector, Color.magenta);
        }
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(ShootRows(playerObject, 3, 3, 0f));
        }
    }
    
    public IEnumerator ShootRows (GameObject player, int rows, int columns, float delay)
    {
        
        float distanceMod = 1.5f;
        for (int j = 0; j < rows; j++)
        {
            for (int i = 0; i < columns; i++)
            {
                Vector3 crossVector = Vector3
                    .Cross(player.transform.position - transform.position, Vector3.up).normalized;
                Debug.Log(crossVector);
                Debug.DrawRay(player.transform.position, crossVector, Color.magenta, 2f);
                Vector3 individualPos = player.transform.TransformVector(new Vector3(crossVector.x + i,
                    crossVector.y + j, crossVector.z));
                //cube.transform.position = player.transform.position + player.transform.TransformVector(individualPos);

                /*GameObject projectile = Instantiate(projectilePrefab, transform.position,
                    Quaternion.LookRotation(player.transform.position
                                            - new Vector3((crossVector.x + (rows * distanceMod)) / 2,
                                                (crossVector.y + (columns * distanceMod)) / 2, 0)
                                            + new Vector3(crossVector.x + (i * distanceMod), 
                                                crossVector.y + (j* distanceMod), crossVector.z)
                                            - transform.position));
                projectile.GetComponent<ProjectileMover>().damage = 0;*/
            }
        }
        yield return new WaitForSeconds(delay);
    }
}

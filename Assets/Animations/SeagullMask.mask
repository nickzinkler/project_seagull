%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: SeagullMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: SEAGULL
    m_Weight: 0
  - m_Path: SEAGULL_
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ L Thigh
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ L Thigh/SEAGULL_ L Calf
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ L Thigh/SEAGULL_ L Calf/SEAGULL_
      L HorseLink
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ L Thigh/SEAGULL_ L Calf/SEAGULL_
      L HorseLink/SEAGULL_ L Foot
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ L Thigh/SEAGULL_ L Calf/SEAGULL_
      L HorseLink/SEAGULL_ L Foot/SEAGULL_ L Toe0
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ L Thigh/SEAGULL_ L Calf/SEAGULL_
      L HorseLink/SEAGULL_ L Foot/SEAGULL_ L Toe0/SEAGULL_ L Toe01
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ L Thigh/SEAGULL_ L Calf/SEAGULL_
      L HorseLink/SEAGULL_ L Foot/SEAGULL_ L Toe0/SEAGULL_ L Toe01/SEAGULL_ L Toe02
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ Head
    m_Weight: 1
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ Head/SEAGULL_
      Queue de cheval 1
    m_Weight: 1
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ L Clavicle
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ L Clavicle/SEAGULL_
      L UpperArm
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ L Clavicle/SEAGULL_
      L UpperArm/SEAGULL_ L Forearm
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ L Clavicle/SEAGULL_
      L UpperArm/SEAGULL_ L Forearm/SEAGULL_ L Hand
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ R Clavicle
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ R Clavicle/SEAGULL_
      R UpperArm
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ R Clavicle/SEAGULL_
      R UpperArm/SEAGULL_ R Forearm
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Neck/SEAGULL_ R Clavicle/SEAGULL_
      R UpperArm/SEAGULL_ R Forearm/SEAGULL_ R Hand
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ R Thigh
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ R Thigh/SEAGULL_ R Calf
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ R Thigh/SEAGULL_ R Calf/SEAGULL_
      R HorseLink
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ R Thigh/SEAGULL_ R Calf/SEAGULL_
      R HorseLink/SEAGULL_ R Foot
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ R Thigh/SEAGULL_ R Calf/SEAGULL_
      R HorseLink/SEAGULL_ R Foot/SEAGULL_ R Toe0
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ R Thigh/SEAGULL_ R Calf/SEAGULL_
      R HorseLink/SEAGULL_ R Foot/SEAGULL_ R Toe0/SEAGULL_ R Toe01
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ R Thigh/SEAGULL_ R Calf/SEAGULL_
      R HorseLink/SEAGULL_ R Foot/SEAGULL_ R Toe0/SEAGULL_ R Toe01/SEAGULL_ R Toe02
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ Spine/SEAGULL_ Tail
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ TailL
    m_Weight: 0
  - m_Path: SEAGULL_/SEAGULL_ Pelvis/SEAGULL_ TailR
    m_Weight: 0

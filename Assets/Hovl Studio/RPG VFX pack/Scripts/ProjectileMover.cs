﻿using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;
using RootMotion.Demos;
using UnityEngine;

public class ProjectileMover : NetworkBehaviour
{
    public float speed = 15f;
    public float hitOffset = 0f;
    public bool UseFirePointRotation;
    public Vector3 rotationOffset = new Vector3(0, 0, 0);
    public GameObject hit;
    public GameObject flash;
    private Rigidbody rb;
    public float damage = 10f;
    public float duration = 15f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (IsOwner)
            SpawnFlashServerRpc();
	}

    [ServerRpc]
    void SpawnFlashServerRpc()
    {
        Destroy(gameObject,5);
        SpawnFlashClientRpc();
    }

    [ClientRpc]
    void SpawnFlashClientRpc()
    {
        if (flash != null)
        {
            var flashInstance = Instantiate(flash, transform.position, Quaternion.identity);
            flashInstance.transform.forward = gameObject.transform.forward;
            var flashPs = flashInstance.GetComponent<ParticleSystem>();
            if (flashPs != null)
            {
                Destroy(flashInstance, flashPs.main.duration);
            }
            else
            {
                var flashPsParts = flashInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(flashInstance, flashPsParts.main.duration);
            }
        }
    }

    void FixedUpdate ()
    {
		if (speed != 0)
        {
            rb.velocity = transform.forward * speed;
            //transform.position += transform.forward * (speed * Time.deltaTime);         
        }
	}

    //https ://docs.unity3d.com/ScriptReference/Rigidbody.OnCollisionEnter.html
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        if (IsHost)
        {
            var health = collision.collider.GetComponent<HealthScript>();
            if (health != null)
            {
                health.TakeDamageServerRpc(damage);
            }
            SpawnHitServerRpc();
        }
    }
    
    [ServerRpc]
    public void SpawnHitServerRpc()
    {
        SpawnHitClientRpc();
        if (hit != null)
        {
            var hitInsance = Instantiate(hit, transform.position, Quaternion.identity);
            hitInsance.GetComponent<NetworkObject>().Spawn();
            Destroy(hitInsance, hitInsance.GetComponent<ParticleSystem>().main.duration);
        }
        Destroy(gameObject);
    }

    [ClientRpc]
    public void SpawnHitClientRpc()
    {
        rb.constraints = RigidbodyConstraints.FreezeAll;
        speed = 0;
    }
}

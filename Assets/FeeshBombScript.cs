using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;

public class FeeshBombScript : NetworkBehaviour
{
    public float damage = 20f;
    public GameObject destroyFX;
    public NetworkVariableBool isActive = new NetworkVariableBool(true);

    [ServerRpc(RequireOwnership = false)]
    public void SetActiveServerRpc()
    {
        isActive.Value = false;
    }
    
    [ServerRpc(RequireOwnership = false)]
    public void DestroyBombServerRpc()
    {
        GameObject fx = Instantiate(destroyFX, transform.position, transform.rotation);
        fx.transform.localScale = new Vector3(2,2,2);
        Destroy(gameObject);
    }
}

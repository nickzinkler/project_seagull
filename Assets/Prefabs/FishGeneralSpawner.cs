using System.Collections;
using System.Collections.Generic;
using MLAPI;
using UnityEngine;

public class FishGeneralSpawner : MonoBehaviour
{
    [SerializeField] private BoxCollider spawnZone;
    [SerializeField] private GameObject fishPrefab;
    [SerializeField] private GameObject appearanceFX;
    
    // Start is called before the first frame update
    void Start()
    {
        spawnZone = GetComponent<BoxCollider>();
    }
    
    public IEnumerator SpawnFish(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            yield return new WaitForSeconds(Random.Range(0f, 1f));
            Bounds bounds = spawnZone.bounds;
            Vector3 spawnPosition = new Vector3(
                Random.Range(bounds.min.x, bounds.max.x),
                Random.Range(bounds.min.y, bounds.max.y),
                Random.Range(bounds.min.z, bounds.max.z)
            );
            Instantiate(appearanceFX, spawnPosition, Quaternion.identity, transform);
            GameObject fish = Instantiate(fishPrefab, spawnPosition, Random.rotation, transform);
            fish.GetComponent<NetworkObject>().Spawn();
        }
    }
}

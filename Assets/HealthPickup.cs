using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;
using UnityEngine;

public class HealthPickup : NetworkBehaviour
{
    private float healAmount = 50f;
    public GameObject model;
    private bool isActive = true;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && isActive)
        {
            other.GetComponent<HealthScript>().HealServerRpc(50f);
            CooldownServerRpc();
        }
    }

    [ServerRpc]
    void CooldownServerRpc()
    {
        CooldownClientRpc();
    }

    [ClientRpc]
    void CooldownClientRpc()
    {
        StartCoroutine(Cooldown());
    }

    IEnumerator Cooldown()
    {
        model.SetActive(false);
        yield return new WaitForSeconds(10f);
        model.SetActive(true);
        isActive = true;
    }
}

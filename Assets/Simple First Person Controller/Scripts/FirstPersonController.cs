﻿using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using MLAPI.Prototyping;
using RootMotion.FinalIK;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(CharacterController))]
public class FirstPersonController : NetworkBehaviour
{
    /// <summary>
    /// Move the player charactercontroller based on horizontal and vertical axis input
    /// </summary>

    public NetworkVariableBool isActive = new NetworkVariableBool(false);

    [FormerlySerializedAs("_playerRenderer")] [SerializeField] private GameObject fullBodyRenderer;
    [SerializeField] private GameObject handsRenderer;
    [SerializeField] private GameObject weaponRoot;
    [SerializeField] private AudioListener _audioListener;
    public GameObject ragdollPrefab;
    private GameObject mainCamera;

    float yVelocity = 0f;
    [Range(5f,25f)]
    public float gravity = 15f;
    //the speed of the player movement
    [Range(5f,15f)]
    public float movementSpeed = 10f;
    //jump speed
    [Range(5f,15f)]
    public float jumpSpeed = 10f;

    //now the camera so we can move it up and down
    [SerializeField]
    private Camera _camera;
    float pitch = 0f;
    [Range(1f,90f)]
    public float maxPitch = 85f;
    [Range(-1f, -90f)]
    public float minPitch = -85f;
    [Range(0.5f, 5f)]
    public float mouseSensitivity = 2f;

    //the charachtercompononet for moving us
    CharacterController cc;

    [FormerlySerializedAs("_anim")] [SerializeField] private Animator _fullbodyAnim;
    [SerializeField] private Animator _handsAnim;

    public NetworkVariableString nickname;
    
    private void Start()
    {
        mainCamera = Camera.main.gameObject;
        if (IsOwner)
            InitialSetupServerRpc();
    }

    [ServerRpc]
    public void InitialSetupServerRpc()
    {
        InitialSetupClientRpc();
    }

    [ClientRpc]
    public void InitialSetupClientRpc()
    {
        _camera.enabled = false;
        fullBodyRenderer.SetActive(false);
        handsRenderer.SetActive(false);
        if (IsServer)
            isActive.Value = false;
        if (IsLocalPlayer)
        {
            cc = GetComponent<CharacterController>();
            cc.enabled = false;
        }
    }
    
    [ServerRpc]
    public void ProcessDamageServerRpc(float health)
    {
        ProcessDamageClientRpc(health);
    }

    [ClientRpc]
    public void ProcessDamageClientRpc(float health)
    {
        if (IsLocalPlayer)
            GameManager.Singleton._uiManager.UpdatePlayerHealth(health);
        if (health <= 0)
        {
            fullBodyRenderer.SetActive(false);
            if (IsLocalPlayer)
            {
                handsRenderer.SetActive(false);
            }

            foreach (var VARIABLE in GetComponentsInChildren<FullBodyBipedIK>())
            {
                VARIABLE.enabled = false;
            }

            gameObject.layer = 9;
            GetComponent<PlayerShooting>().DisableWeapons();
            GetComponent<CharacterController>().enabled = false;
            Debug.Log(transform.position);
            GameObject ragdoll = Instantiate(ragdollPrefab, transform.position, transform.rotation);

            if (IsLocalPlayer)
            {
                StartCoroutine(ReturnToMainCamera(ragdoll));
            }
        }
    }

    IEnumerator ReturnToMainCamera(GameObject ragdoll)
    {
        ragdoll.GetComponentInChildren<Camera>(true).gameObject.SetActive(true);
        yield return new WaitForSeconds(5f);
        GetComponentInChildren<Camera>(true).gameObject.SetActive(false);
        ragdoll.GetComponentInChildren<Camera>().gameObject.SetActive(false);
        mainCamera.SetActive(true);
    }
    
    [ServerRpc]
    public void ActivateVisibilityServerRpc()
    {
        if (IsServer)
            isActive.Value = true;
        SetVisibilityClientRpc();
    }

    [ClientRpc]
    public void SetVisibilityClientRpc()
    {
        weaponRoot.SetActive(true);
        GetComponent<PlayerShooting>().ActivateWeapon();
        if (IsLocalPlayer)
        {
            _camera.enabled = true;
            cc.enabled = true;
            handsRenderer.SetActive(true);
            Cursor.lockState = CursorLockMode.Locked;
            mainCamera.SetActive(false);
            _audioListener.enabled = true;
            HealthScript _health = GetComponent<HealthScript>();
            GameManager.Singleton._uiManager.UpdatePlayerHealth(_health.actualHealth);
        }
        else
        {
            fullBodyRenderer.SetActive(true);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (IsLocalPlayer && isActive.Value && cc != null)
        {
            Look();
            Move();
            
            bool isMoving = cc.velocity.x != 0 || cc.velocity.z != 0;
            _fullbodyAnim.SetBool("isMoving", isMoving);
            _handsAnim.SetBool("isMoving", isMoving);
            
            Physics.Raycast(_camera.transform.position, transform.forward, out RaycastHit hit, 5f);
            if (hit.collider != null && hit.collider.CompareTag("SeagullActivator"))
            {
                SeagullActivatorScript activatorScript = hit.collider.GetComponent<SeagullActivatorScript>();
                if (activatorScript.isActive.Value)
                {
                    if (!GameManager.Singleton._uiManager.objectActivationRoot.activeSelf)
                    {
                        activatorScript.DisplayFirstMessage();
                    }
                    GameManager.Singleton._uiManager.objectActivationRoot.SetActive(true);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        activatorScript.DisplaySecondMessage();
                    }
                }
            }
            else
                GameManager.Singleton._uiManager.objectActivationRoot.SetActive(false);
        }
    }

    void Look()
    {
        //get the mouse inpuit axis values
        float xInput = Input.GetAxis("Mouse X") * mouseSensitivity;
        float yInput = Input.GetAxis("Mouse Y") * mouseSensitivity;
        //turn the whole object based on the x input
        transform.Rotate(0, xInput, 0);
        //now add on y input to pitch, and clamp it
        pitch -= yInput;
        pitch = Mathf.Clamp(pitch, minPitch, maxPitch);
        //create the local rotation value for the camera and set it
        Quaternion rot = Quaternion.Euler(pitch, 0, 0);
        _camera.transform.localRotation = rot;
    }

    void Move()
    {
        //update speed based onn the input
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        input = Vector3.ClampMagnitude(input, 1f);
        //transofrm it based off the player transform and scale it by movement speed
        Vector3 move = transform.TransformVector(input) * movementSpeed;
        //is it on the ground
        if (cc.isGrounded)
        {
            yVelocity = -gravity * Time.deltaTime;
            //check for jump here
            if (Input.GetButtonDown("Jump"))
            {
                yVelocity = jumpSpeed;
            }

            if (jumpRequest)
            {
                yVelocity += Mathf.Sqrt(jumpHeight * -3.0f * -gravity);
                jumpRequest = false;
            }
        }
        //now add the gravity to the yvelocity
        yVelocity -= gravity * Time.deltaTime;
        move.y = yVelocity;
        //and finally move
        cc.Move(move * Time.deltaTime);
    }

    private bool jumpRequest;
    private float jumpHeight;
    
    public void Jump(float jumpspeed)
    {
        Debug.Log("Supposed to jump");
        jumpRequest = true;
        jumpHeight = jumpspeed;
    }

    private bool isVulnerable = true;
    
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.CompareTag("Bomb"))
        {
            FeeshBombScript bomb = hit.collider.GetComponent<FeeshBombScript>();
            if (bomb.isActive.Value && isVulnerable)
            {
                StartCoroutine(VulnerableTimer());
                bomb.SetActiveServerRpc();
                GetComponent<HealthScript>().TakeDamageServerRpc(bomb.damage);
                bomb.DestroyBombServerRpc();
            }
        }
    }

    IEnumerator VulnerableTimer()
    {
        isVulnerable = false;
        yield return new WaitForSeconds(.1f);
        isVulnerable = true;
    }
}

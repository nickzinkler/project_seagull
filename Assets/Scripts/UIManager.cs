using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : NetworkBehaviour
{
    public GameObject startButton;
    public GameObject seagullStatusBar;
    public Image statusBarBackground;
    public Image statusBarForeground;
    public Image statusBarShield;
    public GameObject playersRoot;
    public GameObject currentPlayerDataPrefab;
    public GameObject otherPlayerDataPrefab;
    public TextMeshProUGUI text;
    public GameObject objectActivationRoot;
    public GameObject ammoRoot;
    public TextMeshProUGUI weaponType;
    public TextMeshProUGUI weaponAmmo;

    private PlayerPreviewScript currentPlayerData;
    
    public void setButtonState(bool state)
    {
        startButton.SetActive(state);
    }

    public void SetSeagullStatusBar(bool state)
    {
        seagullStatusBar.SetActive(state);
    }
    
    public void UpdateSeagullHealth(float health, float maxHealth, float shield, float maxShield)
    {
        Vector2 sizeDelta = statusBarBackground.rectTransform.sizeDelta;
        statusBarForeground.rectTransform.sizeDelta =
            new Vector2(sizeDelta.x / maxHealth * health,
                sizeDelta.y);
        statusBarShield.rectTransform.sizeDelta =
            new Vector2(sizeDelta.x / maxShield * shield,
                sizeDelta.y);
        text.text = shield > 0 ? ((int)shield + "/" + maxShield) : ((int)health + "/" + maxHealth);
        }

    public void CreateCurrentPlayerDataPrefab(Texture2D texture, string nickname)
    {
        currentPlayerData = Instantiate(currentPlayerDataPrefab, playersRoot.transform).GetComponent<PlayerPreviewScript>();
        currentPlayerData.profilePicture.texture = texture;
        currentPlayerData.nicknameText.text = nickname;
    }

    public void ActivateHealthData()
    {
        currentPlayerData.healthRoot.SetActive(true);
    }
    
    public void UpdatePlayerHealth(float health)
    {
        Vector2 sizeDelta = currentPlayerData.backgroundHealthbar.rectTransform.sizeDelta;
        currentPlayerData.foregroundHealthbar.rectTransform.sizeDelta =
            new Vector2(sizeDelta.x / 100 * health,
                sizeDelta.y);
        currentPlayerData.healthText.text = health + "/100";
        if (health <= 0)
            currentPlayerData.deadRoot.SetActive(true);
    }

    public void UpdateAmmoData(WeaponType type, int ammo)
    {
        weaponType.text = type.ToString();
        if (type == WeaponType.Pistol)
            weaponAmmo.text = "Ammo: infinite";
        else
            weaponAmmo.text = "Ammo: " + ammo;
    }
}

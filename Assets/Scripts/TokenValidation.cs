using System;
using System.Collections;
using System.Collections.Generic;
using Fernet;
using MLAPI;
using MLAPI.Transports.PhotonRealtime;
using TMPro;
using UnityEngine;

public class TokenValidation : MonoBehaviour
{
    public TextMeshProUGUI statusText;
    public string room;
    
    public void Mirror(string dec)
    {
        if (TryToDecrypt(dec))
        {
            NetworkManager.Singleton.GetComponent<PhotonRealtimeTransport>().RoomName = room;
        }
        else
        {
            room = "";
        }
    }
    
    public bool TryToDecrypt(string dec)
    {
        statusText.text = "";
        SimpleFernet fernet = new SimpleFernet();
        byte[] decoded;
        DateTime time;
        try
        {
            decoded = fernet.Decrypt("QRqkmpZ7gx2gTfpBIF9bj8TKst01rUxk3wiPMTPtYK4=".UrlSafe64Decode(),
                dec,
                out time);
        }
        catch
        {
            statusText.color = Color.red;
            statusText.text = "Неправильный токен!";
            return false;
        }
        string decodedString = decoded.UrlSafe64Encode().FromBase64String();
        string[] tokenParams = ValidateString(decodedString);

        if (tokenParams.Length == 0){
            statusText.color = Color.red;
            statusText.text = "Не удалось валидировать токен!";
            return false;
        }

        room = tokenParams[0];
        
        DateTime.TryParse(tokenParams[2], out time);
        if ((DateTime.Today.Date - time).TotalDays >= 2)
        {
            statusText.color = Color.black;
            statusText.text = "Внимание: этому паролю 2 или больше дня. Повтор уже случившихся рейдов не принесёт участникам никакой рыбы.";
            GameManager.Singleton.fishCount = 0;
            GameManager.Singleton.chatId = tokenParams[3];
            return true;
        }
        
        statusText.color = Color.green;
        statusText.text = "Токен принят!";
        GameManager.Singleton.fishCount = Int32.Parse(tokenParams[1]);
        GameManager.Singleton.chatId = tokenParams[3];
        Debug.Log(GameManager.Singleton.fishCount);
        return true;
    }

    public string[] ValidateString(string token)
    {
        var strings = token.Split(',');
        if (strings[0].StartsWith("room") && strings[1].StartsWith("fish") && strings[2].StartsWith("date") && strings[3].StartsWith("chat_id"))
        {
            string[] result = {
                strings[0].Split('=')[1],
                strings[1].Split('=')[1],
                strings[2].Split('=')[1],
                strings[3].Split('=')[1],
            };
            return result;
        }
        return new string[] {};
    }
}

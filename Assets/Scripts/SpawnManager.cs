using System.Collections;
using System.Collections.Generic;
using MLAPI;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject spawnPointsRoot;

    public GameObject getSpawnPoints()
    {
        return spawnPointsRoot.transform.GetChild((NetworkManager.Singleton.ConnectedClients.Count + 1) % 5).gameObject;
    }
}

using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;

public class HealthScript : NetworkBehaviour
{
    public NetworkVariableFloat health = new NetworkVariableFloat();
    public NetworkVariableFloat shield = new NetworkVariableFloat();
    
    public float maxHealth = 1000f;
    public float actualHealth = 1000f;
    public float maxShield = 0f;

    void Start()
    {
        SetupStartHealthServerRpc();
    }
    
    void Update()
    {
        actualHealth = health.Value;
    }

    [ServerRpc(RequireOwnership = false)]
    void SetupStartHealthServerRpc()
    {
        health.Value = maxHealth;
        shield.Value = maxShield;
    }

    [ServerRpc(RequireOwnership = false)]
    public void HealServerRpc(float healAmount)
    {
        health.Value = Mathf.Clamp(health.Value + healAmount, 0, maxHealth);
        GetComponent<FirstPersonController>().ProcessDamageClientRpc(health.Value);
    }
    
    [ServerRpc(RequireOwnership = false)]
    public void TakeDamageServerRpc(float damage)
    {
        if (shield.Value > 0)
            shield.Value = Mathf.Clamp(shield.Value - damage, 0, Mathf.Infinity);
        else 
            health.Value = Mathf.Clamp(health.Value - damage, 0, Mathf.Infinity);
        
        if (CompareTag("Seagull"))
        {
            GetComponent<SeagullController>().ProcessDamageServerRpc(health.Value, shield.Value);
        }

        if (CompareTag("Player"))
        {
            GetComponent<FirstPersonController>().ProcessDamageClientRpc(health.Value);
        }

        if (CompareTag("Bomb"))
        {
            GetComponent<FeeshBombScript>().DestroyBombServerRpc();
        }
    }

    [ServerRpc]
    public void AddShieldServerRpc(float shieldAmount)
    {
        maxShield = shieldAmount;
        shield.Value = shieldAmount;
    }
}

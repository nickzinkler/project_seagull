using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public enum WeaponType
{
    None   = 0,
    
    Pistol = 1,
    SMGA   = 2,
}

public class WeaponScript : MonoBehaviour
{
    public Transform leftHandEffector;
    [FormerlySerializedAs("rightHandEfffector")] public Transform rightHandEffector;
    public WeaponType weaponType;
    public Transform shootTransform;
    public bool canShoot = true;
    public float reloadTime  = 0.5f;
    public float damage = 10f;
    public float ammo = 50f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator Reload()
    {
        canShoot = false;
        ammo -= 1;
        yield return new WaitForSeconds(reloadTime);
        canShoot = true;
    }   
}

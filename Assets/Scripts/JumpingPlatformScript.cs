using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingPlatformScript : MonoBehaviour
{
    public float jumpSpeed = 100f;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);
        if (other.gameObject.CompareTag("Player"))
        {
            other.transform.GetComponent<FirstPersonController>().Jump(jumpSpeed);
        }
    }
}

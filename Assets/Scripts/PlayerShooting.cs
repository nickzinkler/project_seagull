using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MLAPI;
using MLAPI.Messaging;
using RootMotion.FinalIK;
using UnityEngine;

public class PlayerShooting : NetworkBehaviour
{

    [SerializeField] private TrailRenderer bulletTrail;
    [SerializeField] private Transform gunTransform;
    [SerializeField] private WeaponType currentWeaponType;
    [SerializeField] private FullBodyBipedIK handsIK;
    [SerializeField] private FullBodyBipedIK fullBodyIK;
    
    void Update()
    {
        if (IsLocalPlayer)
        {
            if (Input.GetButton("Fire1"))
            {
                WeaponScript currentWeapon = GetComponentsInChildren
                    <WeaponScript>(true).FirstOrDefault(t => t.weaponType == currentWeaponType);
                if (currentWeapon.canShoot)
                {
                    ShootServerRpc();
                    StartCoroutine(currentWeapon.Reload());
                    if (currentWeapon.ammo == 0)
                    {
                        currentWeaponType = WeaponType.Pistol;
                        ActivateWeapon();
                    }
                }
                
                GameManager.Singleton._uiManager.UpdateAmmoData(currentWeapon.weaponType, (int)currentWeapon.ammo);
            }
        }
    }

    [ServerRpc]
    public void pickupSMGAServerRpc()
    {
        pickupSMGAClientRpc();
    }

    [ClientRpc]
    public void pickupSMGAClientRpc()
    {
        currentWeaponType = WeaponType.SMGA;
        foreach (var weapon in transform.GetComponentsInChildren<WeaponScript>(true))
        {
            if (weapon.weaponType == currentWeaponType)
            {
                weapon.ammo += 50;
            }
        }
        ActivateWeapon();
        WeaponScript currentWeapon = GetComponentsInChildren
            <WeaponScript>(true).FirstOrDefault(t => t.weaponType == currentWeaponType);
        GameManager.Singleton._uiManager.UpdateAmmoData(currentWeapon.weaponType, (int)currentWeapon.ammo);
    }

    public void ActivateWeapon()
    {
        foreach (var weapon in transform.GetComponentsInChildren<WeaponScript>(true))
        {
            if (weapon.weaponType == currentWeaponType)
            {
                weapon.gameObject.SetActive(true);
                if (IsLocalPlayer)
                {
                    if (weapon.leftHandEffector == null)
                    {
                        handsIK.solver.leftHandEffector.positionWeight = 0;
                        handsIK.solver.leftHandEffector.rotationWeight = 0;
                        handsIK.solver.leftHandEffector.target = null;
                    }
                    else 
                    {
                        handsIK.solver.leftHandEffector.positionWeight = 1;
                        handsIK.solver.leftHandEffector.rotationWeight = 1;
                        handsIK.solver.leftHandEffector.target = weapon.leftHandEffector;
                    }
                    
                    if (weapon.rightHandEffector == null)
                    {
                        handsIK.solver.rightHandEffector.positionWeight = 0;
                        handsIK.solver.rightHandEffector.rotationWeight = 0;
                        handsIK.solver.rightHandEffector.target = null;
                    }
                    else 
                    {
                        handsIK.solver.rightHandEffector.positionWeight = 1;
                        handsIK.solver.rightHandEffector.rotationWeight = 1;
                        handsIK.solver.rightHandEffector.target = weapon.rightHandEffector;
                    }
                }
                else
                {
                    if (weapon.leftHandEffector == null)
                    {
                        fullBodyIK.solver.leftHandEffector.positionWeight = 0;
                        fullBodyIK.solver.leftHandEffector.rotationWeight = 0;
                        fullBodyIK.solver.leftHandEffector.target = null;
                    }
                    else 
                    {
                        fullBodyIK.solver.leftHandEffector.positionWeight = 1;
                        fullBodyIK.solver.leftHandEffector.rotationWeight = 1;
                        fullBodyIK.solver.leftHandEffector.target = weapon.leftHandEffector;
                    }
                    
                    if (weapon.rightHandEffector == null)
                    {
                        fullBodyIK.solver.rightHandEffector.positionWeight = 0;
                        fullBodyIK.solver.rightHandEffector.rotationWeight = 0;
                        fullBodyIK.solver.rightHandEffector.target = null;
                    }
                    else 
                    {
                        fullBodyIK.solver.rightHandEffector.positionWeight = 1;
                        fullBodyIK.solver.rightHandEffector.rotationWeight = 1;
                        fullBodyIK.solver.rightHandEffector.target = weapon.rightHandEffector;
                    }
                }
                gunTransform = weapon.shootTransform;
            }
            else
                weapon.gameObject.SetActive(false);
        }
    }

    public void DisableWeapons()
    {
        fullBodyIK.solver.leftHandEffector.target = null;
        fullBodyIK.solver.rightHandEffector.target = null;
        foreach (var weapon in transform.GetComponentsInChildren<WeaponScript>())
        {
            weapon.gameObject.SetActive(false);
        }
    }
    
    [ServerRpc]
    void ShootServerRpc()
    {
        if (Physics.Raycast(gunTransform.position, gunTransform.forward, out RaycastHit hit, 200f))
        {
            Debug.Log("hit");
            var enemyHealth = hit.transform.GetComponentInParent<HealthScript>();
            if (enemyHealth != null && hit.collider.CompareTag("Seagull"))
            {
                WeaponScript currentWeapon = GetComponentsInChildren
                    <WeaponScript>(true).FirstOrDefault(t => t.weaponType == currentWeaponType);
                float damage = currentWeapon.damage;
                if (hit.collider.GetComponent<DamageModificator>())
                {
                    damage *= hit.collider.GetComponent<DamageModificator>().mod;
                }
                enemyHealth.TakeDamageServerRpc(damage);
            }
            else if (hit.transform.GetComponent<FeeshBombScript>())
            {
                hit.transform.GetComponent<FeeshBombScript>().DestroyBombServerRpc();
            }
            else if (hit.transform.GetComponent<ProjectileMover>())
            {
                hit.transform.GetComponent<ProjectileMover>().SpawnHitServerRpc();
            }
        }
        ShootClientRpc();
    }

    [ClientRpc]
    void ShootClientRpc()
    {
        Debug.DrawRay(gunTransform.position, gunTransform.forward, Color.red, 200f);
        var bullet = Instantiate(bulletTrail, gunTransform.position, Quaternion.identity);
        bullet.AddPosition(gunTransform.position);
        if (Physics.Raycast(gunTransform.position, gunTransform.forward, out RaycastHit hit, 200f))
        {
            bullet.transform.position = hit.point;
            if (hit.collider.CompareTag("Seagull"))
            {
                Debug.Log(hit.collider.gameObject.name);
            }
        }
        else
        {
            bullet.transform.position = gunTransform.position + (gunTransform.forward * 200f);
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using TeleSharp.TL.Account;
using TgSharp.Core;
using TgSharp.Core.Exceptions;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour
{
    public TelegramManager tmanager;
    
    [SerializeField]
    private GameObject phoneNumberField, authorizationCodeField, passwordField, resendCodeButton;
    public Text statusText;
    
    private string userPhoneNumber, userHash, userAuthCode, userPassword;

    [SerializeField]
    private Dictionary<TokenSourceTypes, CancellationTokenSource> TokenSources = new Dictionary<TokenSourceTypes, CancellationTokenSource>();

    private enum TokenSourceTypes
    {
        phoneNumber       = 1,
        authorizationCode = 2,
        password          = 3,
    }
    
    public async Task ShowPhoneNumberField(TelegramClient client)
    {
        phoneNumberField.SetActive(true);
        statusText.text = "Enter your phone number";
        await WaitForCancellableInput(TokenSourceTypes.phoneNumber);
        phoneNumberField.SetActive(false);
        ShowAuthorizationCodeField(client);
    }

    public async Task ShowAuthorizationCodeField(TelegramClient client)
    {
        resendCodeButton.SetActive(false);
        authorizationCodeField.SetActive(true);
        userHash = await client.SendCodeRequestAsync(userPhoneNumber);

        await WaitForCancellableInput(TokenSourceTypes.authorizationCode);
        try
        {
            await client.MakeAuthAsync(userPhoneNumber, userHash, userAuthCode);
        }
        catch (InvalidPhoneCodeException ex)
        {
            statusText.text = "Wrong authorization code";
            resendCodeButton.SetActive(true);
        }
        catch (CloudPasswordNeededException ex)
        {
            statusText.text = "2FA enabled. Please disable 2FA and restart the app";
            //ShowPasswordField(client, true);
        }
        statusText.text = "";
        authorizationCodeField.SetActive(false);
        tmanager.StartLobby(tmanager.GetClient());
    }

    public async Task ShowPasswordField(TelegramClient client, bool firstResponse)
    {
        if (firstResponse)
        {
            statusText.text = "2FA enabled. Enter your password";
        }

        authorizationCodeField.SetActive(false);
        passwordField.SetActive(true);
        //TLPassword result = await client.GetPasswordSetting();
        await WaitForCancellableInput(TokenSourceTypes.password);
        
        try
        {
            //await client.MakeAuthWithPasswordAsync(result, userPassword);
            passwordField.SetActive(false);
        }
        catch
        {
            statusText.text = "Wrong password. Try again";
            ShowPasswordField(client, true);
        }
        
        authorizationCodeField.SetActive(false);
        tmanager.StartLobby(tmanager.GetClient());
    }

    async Task WaitForCancellableInput(TokenSourceTypes tokenSourceType)
    {
        TokenSources.Add(tokenSourceType, new CancellationTokenSource());
        CancellationTokenSource tokenSource = TokenSources[tokenSourceType];
        try
        {
            await Task.Delay(-1, tokenSource.Token);
        }
        catch
        {
            Debug.Log("Delay cancelled");
        }
    }

    public async void ResendCode()
    {
        TelegramClient client = tmanager.GetClient();
        await client.ConnectAsync();
        ShowAuthorizationCodeField(client);
    }
    
    public void SetPhoneNumber(string submittedNumber)
    {
        userPhoneNumber = submittedNumber;
        TokenSources[TokenSourceTypes.phoneNumber].Cancel();
    }
    
    public void SetAuthorizationCode(string submittedCode)
    {
        userAuthCode = submittedCode;
        TokenSources[TokenSourceTypes.authorizationCode].Cancel();
    }
    
    public void SetAuthorizationPassword(string submittedPassword)
    {
        userPassword = submittedPassword;
        TokenSources[TokenSourceTypes.password].Cancel();
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using FluffyUnderware.DevTools.Extensions;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using RootMotion.FinalIK;
using UnityEngine;

public struct SplineInfo
{
    public CurvySpline Spline;
    public float PositionOnSpline;
    public Vector3 ClosestPointPosition;

    public SplineInfo(CurvySpline spl, float pos, Vector3 vect)
    {
        Spline = spl;
        PositionOnSpline = pos;
        ClosestPointPosition = vect;
    }
    
}

public class SeagullController : NetworkBehaviour
{
    [SerializeField] private GameObject headObject;
    [SerializeField] private BoxCollider groundedCheckCollider;
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private GameObject shootingRoot;
    public bool isGrounded;
    public bool isFalling;
    public float distToGround;
    private Animator _anim;
    private AudioSource _source;
    public SplineController splineController;
    public float fallingSpeed;
    public NetworkVariableBool canAttack = new NetworkVariableBool(true);
    public SeagullSplineController _SplineController;

    [SerializeField] GameObject splineFollowerPrefab;
    public GameObject currentSplineFollower;
    private Tweener tweener;
    private SplineInfo _targetSplineInfo;

    public List<float> stages;
    public float shield = 200f;
    public float timer = 5f;

    public LayerMask raycastLayerMask;

    [SerializeField] private AudioClip squeak;
    [SerializeField] private AudioClip scream;

    public float followSpeed = 11f;
    public float distanceToStopFollowing = 5f;

    public bool isInvincible = false;
    public bool canFall = true;
    public float speedMod = 1f;
    public float attackSpeed = 1f;
    
    public int maxAttackValue = 0;
    public GameObject seagullFX;
    public GameObject bombObject;
    
    void Start()
    {
        _anim = GetComponent<Animator>();
        _source = GetComponent<AudioSource>();
        splineController = GetComponent<SplineController>();
        distToGround = groundedCheckCollider.bounds.extents.y;
    }

    void Update()
    {
        isGrounded = IsGrounded();
        if (currentSplineFollower == null)
            _anim.SetBool("isGrounded", isGrounded);
        if (isFalling)
        {
            transform.position -= Vector3.up * (Time.deltaTime * fallingSpeed);

            if (isGrounded || transform.position.y < -12f)
            {
                isFalling = false;
                return;
            }
        }

        if (IsHost)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                FindPlayerTargetServerRpc();
                timer = Random.Range(4.5f, 5.5f) / attackSpeed;
            }

            if (currentSplineFollower != null)
            {
                FollowSpline();
            }
        }

        var health = GetComponent<HealthScript>();
        if (health.health.Value > health.maxHealth * 0.8f)
        {
            speedMod = 1f;
            attackSpeed = 1.4f;
        }
        else if (health.health.Value > health.maxHealth * 0.6)
        {
            speedMod = 1.1f;
            attackSpeed = 1.7f;
        }
        else if (health.health.Value > health.maxHealth * 0.4)
        {
            speedMod = 1.3f;
            attackSpeed = 2f;
        }
        else if (health.health.Value > health.maxHealth * 0.2)
        {
            speedMod = 1.5f;
            attackSpeed = 2.2f;
        }
        else
        {
            speedMod = 1.6f;
            attackSpeed = 2.5f;
        }

        splineController.Speed = 10f * speedMod;
    }

    private void FollowSpline()
    {
        if (tweener == null)
        {
            tweener = transform.DOMove(currentSplineFollower.transform.position, followSpeed).SetSpeedBased(true);
        }

        if (Vector3.Distance(transform.position, currentSplineFollower.transform.position) >= distanceToStopFollowing)
        {
            tweener.ChangeEndValue(currentSplineFollower.transform.position, true);
        }
        else
        {
            Destroy(currentSplineFollower);
            tweener = null;
            SplineInfo _info = FindClosestPointOnSpline();
            splineController.Spline = _info.Spline;
            splineController.Position = _info.PositionOnSpline;
            splineController.enabled = true;
        }
    }

    bool IsGrounded()
    {
        ;
        Physics.Raycast(transform.TransformPoint(groundedCheckCollider.center), Vector3.down, out RaycastHit hit,
            distToGround + 0.1f);
        return (hit.collider != null && !hit.collider.CompareTag("Seagull"));
    }

    [ServerRpc]
    public void FallServerRpc()
    {
        FallClientRpc();
    }

    [ClientRpc]
    public void FallClientRpc()
    {
        StartCoroutine(FallingCoroutine());
    }

    public IEnumerator FallingCoroutine()
    {
        canFall = false;
        isInvincible = true;
        if (IsServer)
            canAttack.Value = false;
        _source.PlayOneShot(scream);
        _anim.SetTrigger("Fall");
        _anim.SetBool("isFlying", false);
        splineController.enabled = false;
        yield return new WaitForSeconds(.5f);
        transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
        isFalling = true;
        yield return new WaitForSeconds(1f);
        isInvincible = false;
        yield return new WaitForSeconds(9f);
        if (GetComponent<HealthScript>().shield.Value <= 0f)
            StartCoroutine(StandUpCoroutine());
    }

    public IEnumerator StandUpCoroutine()
    {
        _anim.SetTrigger("StandUp");
        isInvincible = true;
        GetComponent<HealthScript>().AddShieldServerRpc(1000f);
        if (IsServer)
            canAttack.Value = true;
        yield return new WaitForSeconds(0.5f);
        _anim.SetBool("isFlying", true);
        yield return new WaitForSeconds(0.5f);
        isInvincible = false;
        _targetSplineInfo = FindClosestPointOnSpline();
        currentSplineFollower =
            Instantiate(splineFollowerPrefab, _targetSplineInfo.ClosestPointPosition, Quaternion.identity);
        SplineController _controller = currentSplineFollower.GetComponent<SplineController>();
        _controller.Spline = _targetSplineInfo.Spline;
        _controller.Position = _targetSplineInfo.PositionOnSpline;
        _controller.Speed = splineController.Speed;
        _anim.SetBool("isGrounded", false);
        canFall = true;
    }

    [ServerRpc]
    public void ProcessDamageServerRpc(float currentHealth, float currentShield)
    {
        if (currentShield <= 0 && canFall)
            FallServerRpc();
        if (currentHealth <= 0)
        {
            SpawnDeathFXClientRpc();
            GameManager.Singleton._Flock._childAmount = 0;
            GameManager.Singleton._fishGeneralSpawner.StartCoroutine(
                GameManager.Singleton._fishGeneralSpawner.SpawnFish(GameManager.Singleton.fishCount));
            Destroy(gameObject);
        }
    }

    [ClientRpc]
    public void SpawnDeathFXClientRpc()
    {
        Instantiate(seagullFX, transform.position, Quaternion.identity);
    }
    
    [ServerRpc]
    public void FindPlayerTargetServerRpc()
    {
        if (canAttack.Value)
        {
            GameObject nearestPlayer = FindNearestPlayer();
            if (nearestPlayer)
            {
                TargetPlayerClientRpc();
                Attack(nearestPlayer);
            }
        }
    }

    [ClientRpc]
    public void TargetPlayerClientRpc()
    {
        GameObject nearestPlayer = FindNearestPlayer();
        if (nearestPlayer)
        {
            GetComponent<LookAtIK>().solver.target = nearestPlayer.transform;
        }
    }

    public GameObject FindNearestPlayer()
    {
        var playersSortedByDistance = GameObject.FindGameObjectsWithTag("Player")
            .Where(t => t.GetComponent<FirstPersonController>().isActive.Value)
            .OrderBy(t => Vector3.Distance(transform.position, t.transform.position));
        foreach (var player in playersSortedByDistance)
        {
            Physics.Raycast(headObject.transform.position,
                player.transform.position + new Vector3(0f, 0.5f, 0f) - headObject.transform.position,
                out RaycastHit hit, 200f, raycastLayerMask);
            if (hit.collider != null && hit.collider.CompareTag("Player"))
            {
                return player;
            }
        }

        return null;
    }

    public SplineInfo FindClosestPointOnSpline()
    {
        SplineInfo bestSplineInfo = new SplineInfo();
        Vector3 bestPoint = Vector3.zero;
        foreach (CurvySpline spline in _SplineController.splines)
        {
            Vector3 localSplinePosition = spline.transform.InverseTransformPoint(transform.position);
            float position = spline.GetNearestPointTF(localSplinePosition, out Vector3 point);
            point += spline.transform.localPosition;
            if (bestSplineInfo.Spline == null ||
                Vector3.Distance(localSplinePosition, point) <
                Vector3.Distance(localSplinePosition, bestSplineInfo.ClosestPointPosition))
            {
                bestSplineInfo = new SplineInfo(spline, position, point);
            }
        }

        return bestSplineInfo;
    }

    public void Attack(GameObject player)
    {
        _source.PlayOneShot(squeak);
        _anim.SetTrigger("Shoot");

        var health = GetComponent<HealthScript>();

        if (health.health.Value < health.maxHealth * 1f)
            maxAttackValue = 0;
        if (health.health.Value < health.maxHealth * 0.8f)
            maxAttackValue = 1;
        if (health.health.Value < health.maxHealth * 0.6f)
            maxAttackValue = 2;
        if (health.health.Value < health.maxHealth * 0.4f)
            maxAttackValue = 3;
        if (health.health.Value < health.maxHealth * 0.2f)
            maxAttackValue = 4;

        switch (Random.Range(0, maxAttackValue+1))
        {
            case 0:
                StartCoroutine(SimpleShoot(player, 3, 0.2f));
                break;
            case 1:
                StartCoroutine(DropBomb(player, 3, 0.1f));
                break;
            case 2:
                StartCoroutine(SimpleShoot(player, 6, 0.1f));
                break;
            case 3:
                StartCoroutine(ShootRows(player, 3, 3, 0));
                break;
            case 4:
                StartCoroutine(ShootRows(player, 6, 6, 0));
                break;
            default:
                StartCoroutine(SimpleShoot(player, 3, 0.2f));
                break;
        }
    }

    public IEnumerator SimpleShoot(GameObject player, int times, float delay)
    {
        for (int i = 0; i < times; i++)
        {
            GameObject projectile = Instantiate(projectilePrefab, shootingRoot.transform.position,
                Quaternion.LookRotation(player.transform.position + new Vector3(0f, 0.5f, 0f) -
                                        shootingRoot.transform.position));
            projectile.GetComponent<NetworkObject>().Spawn();
            projectile.GetComponent<ProjectileMover>().speed *= speedMod; 
            yield return new WaitForSeconds(delay);
        }
    }
    
    public IEnumerator ShootRows (GameObject player, int rows, int columns, float delay)
    {
        float distanceMod = 1.5f;
        for (int j = 0; j < rows; j++)
        {
            for (int i = 0; i < columns; i++)
            {
                Vector3 crossVector = Vector3
                    .Cross(player.transform.position - shootingRoot.transform.position, Vector3.up).normalized;
                Vector3 individualPos = player.transform.TransformVector(new Vector3(crossVector.x + (i*distanceMod),
                    crossVector.y + (j*distanceMod), crossVector.z));
                GameObject projectile = Instantiate(projectilePrefab, shootingRoot.transform.position,
                    Quaternion.LookRotation(player.transform.position
                                            + individualPos
                                            - shootingRoot.transform.position));
                projectile.GetComponent<NetworkObject>().Spawn();
                projectile.GetComponent<ProjectileMover>().speed *= speedMod; 
            }
        }
        yield return new WaitForSeconds(delay);
    }
    
    public IEnumerator DropBomb(GameObject player, int times, float delay)
    {
        for (int i = 0; i < times; i++)
        {
            GameObject projectile =
                Instantiate(bombObject, groundedCheckCollider.transform.position, transform.rotation);
            projectile.GetComponent<NetworkObject>().Spawn();
            yield return new WaitForSeconds(delay);
        }
    }
}

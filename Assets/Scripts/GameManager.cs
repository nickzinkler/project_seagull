    using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using MLAPI;
using MLAPI.Messaging;
using UnityEngine;

public class GameManager : NetworkBehaviour
{
    private NetworkManager _networkManager;
    private NetworkObject _playerObject;
    [SerializeField] private GameObject seagullPrefab;
    public GameObject seagull;

    [SerializeField] public UIManager _uiManager;
    [SerializeField] private SeagullSplineController _splineController;
    [SerializeField] public FishGeneralSpawner _fishGeneralSpawner;
    [SerializeField] public FlockController _Flock;
    public string username;
    public int fishCount;
    public string chatId;

    public static GameManager Singleton { get; private set; }
    
    private void OnEnable()
    {
        if (Singleton != null && Singleton != this)
        {
            Destroy(gameObject);
            return;
        }

        Singleton = this;
    }

    private void OnDestroy()
    {
        if (Singleton != null && Singleton == this)
        {
            Singleton = null;
        }
    }
    
    private void Start()
    {
        _networkManager = NetworkManager.Singleton;
    }
    
    private void Update()
    {
        if (seagull)
        {
            HealthScript health = seagull.GetComponent<HealthScript>();
            _uiManager.UpdateSeagullHealth(health.actualHealth, health.maxHealth, health.shield.Value, health.maxShield);
        }
    }

    public void ActivatePlayerPrefab()
    {
        _playerObject = _networkManager
            .ConnectedClients[_networkManager.LocalClientId].PlayerObject;
        _playerObject.GetComponent<FirstPersonController>().ActivateVisibilityServerRpc();
        _uiManager.ammoRoot.SetActive(true);
    }

    [ServerRpc]
    public void SpawnSeagullServerRpc()
    {
        seagull = Instantiate(seagullPrefab);
        seagull.GetComponent<NetworkObject>().Spawn();
        SpawnSeagullClientRpc();
        _Flock._childAmount = 250;
    }

    [ClientRpc]
    public void SpawnSeagullClientRpc()
    {
        if (seagull == null)
        {
            seagull = GameObject.FindGameObjectsWithTag("Seagull")
                .FirstOrDefault(t => t.GetComponent<NetworkObject>());
        }

        seagull.GetComponent<SeagullController>().splineController.Spline = _splineController.startSpline;
        seagull.GetComponent<SeagullController>()._SplineController = _splineController;
        seagull.GetComponent<SeagullController>().splineController.enabled = true;
        seagull.GetComponent<SeagullController>().speedMod = 10f;
        _uiManager.SetSeagullStatusBar(true);
    }

    public void FinishRaid()
    {
        var handler = new HttpClientHandler();
        using (var httpClient = new HttpClient(handler))
        {
            using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://fisher-king.herokuapp.com/457399966:AAHqGhrxQhRCR8OwdMox3hmhBlK5yDmikSE"))
            {
                request.Headers.TryAddWithoutValidation("Cache-Control", "no-cache"); 

                request.Content = new StringContent("{\n\"update_id\":10000,\n\"message\":{\n  \"date\":1441645532,\n  \"chat\":{\n     \"last_name\":\"Test Lastname\",\n     " +
                                                    "\"id\":"+"-1001190496039"+",\n     \"first_name\":\"Test\",\n     " +
                                                    "\"username\":\"" + username + "\",\n     \"type\":\"private\"\n  },\n  \"message_id\":1365,\n  \"from\":{\n     \"last_name\":\"Test Lastname\",\n     \"id\":2222222,\n     \"is_bot\":false,\n     \"first_name\":\"Test\",\n     " +
                                                    "\"username\":\"" + username + "\"\n  },\n  " +
                                                    "\"text\":\"" + chatId + "\"\n}\n}");
                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json"); 

                var response = httpClient.SendAsync(request);
            }
        }
    }
}

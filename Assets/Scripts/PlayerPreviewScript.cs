using System.Collections;
using System.Collections.Generic;
using MLAPI;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPreviewScript : NetworkBehaviour
{
    public RawImage profilePicture;
    public Text nicknameText;
    public Text healthText;
    public GameObject healthRoot;
    public Image backgroundHealthbar, foregroundHealthbar;
    public GameObject deadRoot;

    public void SetupPlayerData(Texture2D photo)
    {
        profilePicture.texture = photo;
        nicknameText.text = NetworkManager.Singleton.ConnectedClients[NetworkManager.Singleton.LocalClientId]
            .PlayerObject.GetComponent<FirstPersonController>().nickname.Value;
    }

    public void SetupOtherPlayerData()
    {
        
    }
    
    public void UpdateHealth(float curHealth, float maxHealth)
    {
        Vector2 sizeDelta = backgroundHealthbar.rectTransform.sizeDelta;
        foregroundHealthbar.rectTransform.sizeDelta =
            new Vector2(sizeDelta.x / maxHealth * curHealth,
                sizeDelta.y);
    }
}

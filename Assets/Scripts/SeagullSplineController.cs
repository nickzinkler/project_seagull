using System.Collections;
using System.Collections.Generic;
using FluffyUnderware.Curvy;
using UnityEngine;

public class SeagullSplineController : MonoBehaviour
{
    public CurvySpline startSpline;
    public CurvySpline[] splines;
    
    // Start is called before the first frame update
    void Start()
    {
        splines = GetComponentsInChildren<CurvySpline>();
    }
}

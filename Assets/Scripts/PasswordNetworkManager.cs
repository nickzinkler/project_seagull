using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.Transports.PhotonRealtime;
using MLAPI.Transports.Tasks;
using Photon.Realtime;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class PasswordNetworkManager : NetworkBehaviour
{
    [SerializeField] private GameObject passwordEntryUI;
    [SerializeField] private GameObject leaveButton;
    NetworkManager _networkManager;
    private PhotonRealtimeTransport _photon;
    private SpawnManager _spawnManager;
    public TextMeshProUGUI statusText;
    [SerializeField] private UIManager _uiManager;
    public int totalConnectionNumber = 0;
    
    
    private void Start()
    {
        _networkManager = NetworkManager.Singleton;
        _photon = _networkManager.GetComponent<PhotonRealtimeTransport>();
        _spawnManager = _networkManager.GetComponent<SpawnManager>();
        _networkManager.OnServerStarted += HandleServerStarted;
        _networkManager.OnClientConnectedCallback += HandleClientConnected;
        _networkManager.OnClientDisconnectCallback += HandleClientDisconnect;
    }

    private void OnDestroy()
    {
        if (_networkManager == null)
        {
            return;}
        
        _networkManager.OnServerStarted += HandleServerStarted;
        _networkManager.OnClientConnectedCallback += HandleClientConnected;
        _networkManager.OnClientDisconnectCallback += HandleClientDisconnect;
    }

    public void Host()
    {
        if (_photon.RoomName != "")
        {
            _networkManager.ConnectionApprovalCallback += ApprovalCheck;
            GameObject spawnpoints = _spawnManager.getSpawnPoints();
            _networkManager.StartHost(spawnpoints.transform.position, spawnpoints.transform.rotation);
        }
        else
        {
            statusText.color = Color.red;
            statusText.text = "Неправильный токен!";
        }
    }

    public void Client()
    {
        if (_photon.RoomName != "")
        {
            _networkManager.StartClient();
        }
        else
        {
            statusText.color = Color.red;
            statusText.text = "Неправильный токен!";
        }
    }

    public void Leave()
    {
        if (_networkManager.IsHost)
        {
            _networkManager.StopHost();
            _networkManager.ConnectionApprovalCallback -= ApprovalCheck;
        }
        else if (_networkManager.IsClient)
        {
            _networkManager.StopClient();
            passwordEntryUI.SetActive(true);
        }
    }
    
    private void ApprovalCheck(byte[] connectionData, ulong clientId, NetworkManager.ConnectionApprovedDelegate callback)
    {
        GameObject spawnpoints = _spawnManager.getSpawnPoints();
        bool result = totalConnectionNumber < 4;
        if (!result)
        {
            statusText.color = Color.red;
            statusText.text = "The raid is already full. Only 5 players are allowed per raid";
        }
        callback(true, null, result, spawnpoints.transform.position, spawnpoints.transform.rotation);
    }
    
    private void HandleServerStarted()
    {
        if (_networkManager.IsHost)
        {
            HandleClientConnected(_networkManager.LocalClientId);
            _uiManager.startButton.SetActive(true);
        }
    }
    
    private void HandleClientConnected(ulong clientId)
    {
        if (clientId == _networkManager.LocalClientId)
        {
            passwordEntryUI.SetActive(false);
            //_uiManager.SetSeagullStatusBar(true);
            _uiManager.startButton.SetActive(true);
            _uiManager.ActivateHealthData();
            /*foreach (var obj in GameObject.FindGameObjectsWithTag("Seagull"))
            {
                if (obj.GetComponent<NetworkObject>())
                {
                    GameManager.Singleton.seagull = obj;
                    return;
                }
            }*/
        }

        totalConnectionNumber++;
    }
    
    private void HandleClientDisconnect(ulong clientId)
    {
        if (clientId == _networkManager.LocalClientId)
        {
            passwordEntryUI.SetActive(true);
            statusText.color = Color.yellow;
            statusText.text = "Disconnected";
        }
    }
}

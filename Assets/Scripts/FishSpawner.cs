using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using DG.Tweening;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using UnityEngine;

public class FishSpawner : NetworkBehaviour
{
    private HttpClient client = new HttpClient();
    private string url = "https://fisher-king.herokuapp.com/457399966:AAHqGhrxQhRCR8OwdMox3hmhBlK5yDmikSE";
    [SerializeField] private GameObject MarkerFX;
    private GameObject connectedFX;
    public NetworkVariableBool isObtainable = new NetworkVariableBool(true);
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DisplayFX());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DisplayFX()
    {
        yield return new WaitForSeconds(5f);
        connectedFX = Instantiate(MarkerFX, transform.position, Quaternion.Euler(0, 0, 0));
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Destroy(connectedFX);
            DestroyFishServerRpc(GameManager.Singleton.username);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    void DestroyFishServerRpc(string username)
    {
        if (isObtainable.Value)
        {
            isObtainable.Value = false;
            SendFish(username, 1, GameManager.Singleton.chatId);
            if (FindObjectsOfType<FishSpawner>().Length == 1)
            {
                GameManager.Singleton.FinishRaid();
            }
            Destroy(gameObject);
        }
    }
    
    public void SendFish(string username, int fishAmount, string chatId)
    {
        var handler = new HttpClientHandler();
        using (var httpClient = new HttpClient(handler))
        {
            using (var request = new HttpRequestMessage(new HttpMethod("POST"), "https://fisher-king.herokuapp.com/457399966:AAHqGhrxQhRCR8OwdMox3hmhBlK5yDmikSE"))
            {
                request.Headers.TryAddWithoutValidation("Cache-Control", "no-cache"); 

                request.Content = new StringContent("{\n\"update_id\":10000,\n\"message\":{\n  \"date\":1441645532,\n  \"chat\":{\n     \"last_name\":\"Test Lastname\",\n     " +
                                                    "\"id\":"+"-1001190496039"+",\n     \"first_name\":\"Test\",\n     " +
                                                    "\"username\":\"" + username + "\",\n     \"type\":\"private\"\n  },\n  \"message_id\":1365,\n  \"from\":{\n     \"last_name\":\"Test Lastname\",\n     \"id\":1111111,\n     \"is_bot\":false,\n     \"first_name\":\"Test\",\n     " +
                                                    "\"username\":\"" + username + "\"\n  },\n  " +
                                                    "\"text\":\"" + chatId + "\"\n}\n}");
                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json"); 

                var response = httpClient.SendAsync(request);
            }
        }
    }
}

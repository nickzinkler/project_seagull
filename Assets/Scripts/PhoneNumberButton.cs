using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneNumberButton : MonoBehaviour
{
    public LoginManager loginManager;
    public InputField input;

    public void AuthByNumber()
    {
        Debug.Log("Button pressed");
        loginManager.SetPhoneNumber(input.text);
    }

    public void EnterCode()
    {
        loginManager.SetAuthorizationCode(input.text);
    }

    public void EnterPassword()
    {
        loginManager.SetAuthorizationPassword(input.text);
    }

    public void ResendCode()
    {
        loginManager.ResendCode();
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using TeleSharp.TL.Account;
using TgSharp.Core;
using TgSharp.Core.Exceptions;
using TgSharp.TL;
using UnityEngine.UI;

public class TelegramManager : MonoBehaviour
{
    private TelegramClient NewClient()
    {
        try
        {
            return new TelegramClient(apiId, apiHash, dcIpVersion: DataCenterIPVersion.PreferIPv4);
        }
        catch (MissingApiConfigurationException ex)
        {
            throw new Exception($"Please add your API settings to the `app.config` file. (More info: {MissingApiConfigurationException.InfoUrl})",
                ex);
        }
    }
    
    public TelegramClient GetClient()
    {
        return NewClient();
    }
    
    private int apiId = 6178543;
    private string apiHash = "3666ca373c90708829a6c122d1c512b9";
    private string code = "";
    private string password = "";

    [SerializeField] private GameObject loginManagerPrefab;
    [SerializeField] private GameObject lobbyPrefab;
    [SerializeField] private GameObject lobbyObject;
    [SerializeField] private LoginManager _loginManager;
    private PasswordNetworkManager _passwordNetworkManager;
    private TokenValidation _tokenValidation;
    [SerializeField] private GameObject StartButton;
    
    [SerializeField]
    private Text greetingText;
    
    [SerializeField]
    private RawImage image;

    [SerializeField] private Canvas mainCanvas;
    
    private async void Start()
    {
        
        var client = NewClient();
        await client.ConnectAsync();
        if (!client.IsUserAuthorized())
        {
            StartAutorization(client);
        }
        else
        {
            StartLobby(client);            
        }
    }

    public async void StartAutorization(TelegramClient client)
    {
        _loginManager.gameObject.SetActive(true);
        _loginManager.tmanager = this;
        _loginManager.ShowPhoneNumberField(client);
    }
    
    public async void StartLobby(TelegramClient client)
    {
        lobbyObject.SetActive(true);
        _passwordNetworkManager = lobbyObject.GetComponent<PasswordNetworkManager>();
        _tokenValidation = lobbyObject.GetComponent<TokenValidation>();
        await client.ConnectAsync();
        if (!client.IsUserAuthorized())
        {
            return;
        }
       string playerUsername = client.Session.TLUser.Username;

        var photo = (TLUserProfilePhoto) client.Session.TLUser.Photo;
        var location = (TLFileLocationToBeDeprecated) photo.PhotoSmall;
        var request = new TLInputPeerPhotoFileLocation()
        {
            Big = true,
            VolumeId = location.VolumeId,
            LocalId = location.LocalId,
            Peer = new TLInputPeerSelf()
        };
        var buffer = await client.GetFile(request, 1024 * 512);
        Texture2D playerAvatar = bytesToTexture2D(buffer.Bytes);
        GameManager.Singleton._uiManager.CreateCurrentPlayerDataPrefab(playerAvatar, playerUsername);
        GameManager.Singleton.username = client.Session.TLUser.Username;
    }
    
    public Texture2D bytesToTexture2D(byte[] imageBytes)
    {
        Texture2D tex = new Texture2D(2, 2);
        tex.LoadImage(imageBytes);
        return tex;
    }
}

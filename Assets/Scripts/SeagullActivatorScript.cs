using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using TMPro;
using UnityEngine;

public class SeagullActivatorScript : NetworkBehaviour
{
    public NetworkVariableBool isActive = new NetworkVariableBool(true);
    public int FishCount = 0;
    public GameObject fx;

    public void DisplayFirstMessage()
    {
        FishCount = GameManager.Singleton.fishCount;
        if (FishCount == 0)
        {
            GameManager.Singleton._uiManager.objectActivationRoot.GetComponentInChildren<TextMeshProUGUI>().text =
                "This fight is in the days gone\n" +
                "The fish is gone, and you are only here for the glory.\n" +
                "If you want to continue, press E to summon the Seagull";
        }
        else 
            GameManager.Singleton._uiManager.objectActivationRoot.GetComponentInChildren<TextMeshProUGUI>().text =
            "Welcome, islanders!\n" +
            "You are here to fight the mighty Seagull for " + FishCount + " fish.\n" +
            "Press E to summon the Seagull";
    }

    public void DisplaySecondMessage()
    {
        ActivateSeagullServerRpc();
    }
    
    [ServerRpc(RequireOwnership = false)]
    public void ActivateSeagullServerRpc()
    {
        ActivateSeagullClientRpc();
        isActive.Value = false;
        GameManager.Singleton.SpawnSeagullServerRpc();
        GetComponentInChildren<AudioSource>().Play();
    }

    [ClientRpc]
    public void ActivateSeagullClientRpc()
    {
        GameManager.Singleton._uiManager.objectActivationRoot.GetComponentInChildren<TextMeshProUGUI>().text =
            "Seagull summoned. So help you God";
        StartCoroutine(Deactivate());
    }

    IEnumerator Deactivate()
    {
        fx.SetActive(false);
        yield return new WaitForSeconds(3f);
        GameManager.Singleton._uiManager.objectActivationRoot.GetComponentInChildren<TextMeshProUGUI>().text = "";
    }
}
